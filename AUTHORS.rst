# Credits

## Development Lead

* Behavioral Signals <nassos@behavioralsignals.com>

## Contributors
* Dimopoulos Spyros
* Katsamanis Nassos
* Sotiri Ledion
* Manolis M. Tsangaris
