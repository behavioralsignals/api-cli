# -*- coding: utf-8 -*-

"""Console script for behavioral_signals_cli."""

# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4:

import click


@click.command()
def main(args=None):
    """Console script for behavioral_signals_cli."""
    click.echo("Replace this message by putting your code into "
               "behavioral_signals_cli.cli.main")
    click.echo("See click documentation at http://click.pocoo.org/")


if __name__ == "__main__":
    main()
