# -*- coding: utf-8 -*-
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4:

"""Top-level package for Behavioral Signals CLI."""

__author__ = """Behavioral Signals"""
__email__ = 'nassos@behavioralsignals.com'
__version__ = '1.6.2'
